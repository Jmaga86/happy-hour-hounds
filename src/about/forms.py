from django import forms
from datetimewidget.widgets import DateTimeWidget, DateWidget, TimeWidget
from pagedown.widgets import PagedownWidget

from .models import About


class AboutForm(forms.ModelForm):
	content = forms.CharField(widget=PagedownWidget(show_preview=False))
	class Meta:
		model = About
		fields = [
			"title",
			"content",
			"image",
			"alt_text",
			"align",
		]