from django.conf.urls import url
from django.contrib import admin

from .views import (
	about_list,
	about_create,
	about_detail,
	about_update,
	about_delete,
	)

urlpatterns = [
	url(r'^create/', about_create, name='create'),
	url(r'^(?P<pk>\d+)/$', about_detail, name='detail'),
	url(r'^(?P<pk>\d+)/edit/$', about_update, name='update'),
	url(r'^(?P<pk>\d+)/delete/$', about_delete, name='delete'),
	url(r'^$', about_list, name='list'),
]