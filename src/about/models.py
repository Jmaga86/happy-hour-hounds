from __future__ import unicode_literals

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.utils import timezone
from django.utils.safestring import mark_safe

from markdown_deux import markdown

class AboutManager(models.Manager):
    def align(self, *args, **kwargs):
        return super(AboutManager, self).filter(align=True)

def upload_location(instance, filename):
	AboutModel = instance.__class__
	new_id = AboutModel.objects.order_by("id").last().id + 1
	return "%s/%s" %(new_id, filename)

class Alignment(models.Model):
	alignment = models.CharField(max_length=5)

	def __unicode__(self):
		return self.alignment

	def __str__(self):
		return self.alignment
	
	

class About(models.Model):
	title = models.CharField(max_length=120)
	image = models.ImageField(upload_to=upload_location, 
			null=True, 
			blank=True, 
			width_field="width_field", 
			height_field="height_field")
	height_field = models.IntegerField(default=0)
	width_field = models.IntegerField(default=0)
	alt_text = models.CharField(max_length=255, null=True, blank=True)
	content = models.TextField()
	alignment = models.ForeignKey(Alignment, blank=True, null=True, on_delete=models.CASCADE)
	align = models.BooleanField(default=False)
	updated = models.DateTimeField(auto_now=True, auto_now_add=False)
	timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

	objects = AboutManager()

	def get_absolute_url(self):
		return reverse("about:detail", kwargs={"pk": self.pk})

	def __unicode__(self):
		return self.title

	def __str__(self):
		return self.title

		
	def get_markdown(self):
		content = self.content
		markdown_text = markdown(content)
		return mark_safe(markdown_text)



