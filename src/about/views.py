try:
    from urllib import quote_plus #python 2
except:
    pass

try:
    from urllib.parse import quote_plus #python 3
except: 
    pass

from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from django.views.generic import View

from .forms import AboutForm
from .models import About

def about_create(request):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404
		
	form = AboutForm(request.POST or None, request.FILES or None)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.user = request.user
		instance.save()
		# message success
		messages.success(request, "Successfully Created")
		return redirect('about:list')
	context = {
		"form": form,
	}
	return render(request, "about/about_form.html", context)

def about_detail(request, pk=None):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404    
	instance = get_object_or_404(About, pk=pk)

	context = {
		"title": instance.title,
		"instance": instance,

	}
	return render(request, "about/about_detail.html", context)

def about_list(request):
	queryset = About.objects.all().order_by('pk')
	context = {
		"object_list": queryset,
		"title": "About Us",
	}
	return render(request, "about/about_list.html", context)





def about_update(request, pk=None):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404
	instance = get_object_or_404(About, pk=pk)
	form = AboutForm(request.POST or None, request.FILES or None, instance=instance)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		messages.success(request, "<a href='#'>Item</a> Saved", extra_tags='html_safe')
		return redirect('about:list')

	context = {
		"title": instance.title,
		"instance": instance,
		"form":form,
	}
	return render(request, "about/about_form.html", context)



def about_delete(request, pk=None):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404
	instance = get_object_or_404(About, pk=pk)
	if request.method =="POST":
		instance.delete()
		messages.success(request, "Successfully deleted")
		return redirect('about:list')
	context = {
		"object": instance
	}
	return render(request,"about/confirm_delete.html", context)
