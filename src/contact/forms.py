from django import forms
from .models import Contact

class ContactForm(forms.ModelForm):
	class Meta:
		model = Contact
		fields = [
			"email",
			"message",
		]
		widgets = {
			'message': forms.Textarea(attrs={'cols':120, 'rows':20}),
		}