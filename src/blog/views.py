try:
    from urllib import quote_plus #python 2
except:
    pass

try:
    from urllib.parse import quote_plus #python 3
except: 
    pass
from datetime import date
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from django.views.generic import View

from .forms import PostForm
from .models import Post
from places.models import Place, Event

class HomeView(View):
	def get(self, request, *args, **kwargs):
		today = date.today()
		if not request.user.is_staff or not request.user.is_superuser:
			featured = Place.objects.filter(event__featured=True)
			sponsored = Place.objects.filter(event__sponsored=True)
			posts = Post.objects.post().filter(draft=False).order_by("-timestamp")[:3]
			event_posts = Post.objects.event().filter(draft=False).order_by("-timestamp")[:3]			
			events = Event.objects.all().order_by('-pk')[0:3]
			hhours = Place.objects.filter(
					Q(event__happy_hour=True)|
					Q(event__special=True)
					).distinct().order_by('-pk')[0:3]	
		else:
			featured = Place.objects.filter(event__featured=True)
			sponsored = Place.objects.filter(event__sponsored=True)
			posts = Post.objects.post().order_by("-timestamp")[:3]
			event_posts = Post.objects.event().order_by("-timestamp")[:3]			
			events = Event.objects.all().order_by('-pk')[0:3]
			hhours = Place.objects.filter(
					Q(event__happy_hour=True)|
					Q(event__special=True)
					).distinct().order_by('-pk')[0:3]	
			# hhours = Event.objects.hour().order_by('-pk')[0:3]			
		context = {
			"featured": featured,
			"sponsored": sponsored,
			"event_posts": event_posts,			
			"posts": posts,
			"events": events,
			"hhours": hhours,
		}
		return render(request, "home.html", context)

def post_create(request):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404
		
	form = PostForm(request.POST or None, request.FILES or None)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.user = request.user
		instance.save()
		# message success
		messages.success(request, "Successfully Created")
		return HttpResponseRedirect(instance.get_absolute_url())
	context = {
		"form": form,
	}
	return render(request, "blog/post_form.html", context)

def post_detail(request, slug=None):
	instance = get_object_or_404(Post, slug=slug)
	if instance.publish > timezone.now().date() or instance.draft:
		if not request.user.is_staff or not request.user.is_superuser:
			raise Http404
	share_string = quote_plus(instance.content)
	context = {
		"title": instance.title,
		"instance": instance,
		"share_string": share_string,
	}
	return render(request, "blog/post_detail.html", context)

def post_list(request):
	today = timezone.now().date()
	queryset_list = Post.objects.post().filter(draft=False) #.order_by("-timestamp")
	if request.user.is_staff or request.user.is_superuser:
		queryset_list = Post.objects.post()
	
	query = request.GET.get("q")
	if query:
		queryset_list = Post.objects.post().filter(
				Q(title__icontains=query)|
				Q(content__icontains=query)|
				Q(user__first_name__icontains=query) |
				Q(user__last_name__icontains=query)
				).distinct()
	paginator = Paginator(queryset_list, 8) # Show 25 contacts per page
	page_request_var = "page"
	page = request.GET.get(page_request_var)
	try:
		queryset = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		queryset = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		queryset = paginator.page(paginator.num_pages)


	context = {
		"object_list": queryset, 
		"title": "Posts",
		"page_request_var": page_request_var,
		"today": today,
	}
	return render(request, "blog/post_list.html", context)

def event_list(request):
	today = timezone.now().date()
	queryset_list = Post.objects.event().filter(draft=False) #.order_by("-timestamp")
	if request.user.is_staff or request.user.is_superuser:
		queryset_list = Post.objects.event()
	
	query = request.GET.get("q")
	if query:
		queryset_list = Post.objects.event().filter(
				Q(title__icontains=query)|
				Q(content__icontains=query)|
				Q(user__first_name__icontains=query) |
				Q(user__last_name__icontains=query)
				).distinct()
	paginator = Paginator(queryset_list, 8) # Show 25 contacts per page
	page_request_var = "page"
	page = request.GET.get(page_request_var)
	try:
		queryset = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		queryset = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		queryset = paginator.page(paginator.num_pages)


	context = {
		"object_list": queryset, 
		"title": "Events",
		"page_request_var": page_request_var,
		"today": today,
	}
	return render(request, "blog/post_list.html", context)



def post_update(request, slug=None):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404
	instance = get_object_or_404(Post, slug=slug)
	form = PostForm(request.POST or None, request.FILES or None, instance=instance)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		messages.success(request, "<a href='#'>Item</a> Saved", extra_tags='html_safe')
		return HttpResponseRedirect(instance.get_absolute_url())

	context = {
		"title": instance.title,
		"instance": instance,
		"form":form,
	}
	return render(request, "blog/post_form.html", context)



def post_delete(request, slug=None):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404
	instance = get_object_or_404(Post, slug=slug)
	if request.method =="POST":
		instance.delete()
		messages.success(request, "Successfully deleted")
		return redirect('posts:list')
	context = {
		"object": instance
	}
	return render(request,"blog/confirm_delete.html", context)