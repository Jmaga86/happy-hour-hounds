from django import forms
from datetimewidget.widgets import DateTimeWidget, DateWidget, TimeWidget
from pagedown.widgets import PagedownWidget

from .models import Post


class PostForm(forms.ModelForm):
    content = forms.CharField(widget=PagedownWidget(show_preview=False))
    class Meta:
        model = Post
        fields = [
            "title",
            "content",
            "image",
            "alt_text",
            "draft",
            "publish",
            "event",
        ]
        widgets = {
            'publish': DateWidget(attrs={'class':'datepicker'}, usel10n = True, bootstrap_version=3),
            
        }
        
