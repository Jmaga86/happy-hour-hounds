"""hounds URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
	https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
	1. Add an import:  from my_app import views
	2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
	1. Add an import:  from other_app.views import Home
	2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
	1. Import the include() function: from django.conf.urls import url, include
	2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.views.generic.base import TemplateView
import places.views
import contact.views
from blog.views import HomeView
from allauth.account import views
from axes.decorators import watch_login

urlpatterns = [
	url(r'^$', HomeView.as_view(), name='home'),
	url(r'^admin/', admin.site.urls),
	url(r'^posts/', include("blog.urls", namespace='posts')),
	url(r'^events/', include("places.urls", namespace='events')),
	url(r'^about/', include("about.urls", namespace='about')),
	url(r'^contact/', contact.views.contact, name='contact'),
	url(r'^fetch/$', places.views.fetch_near, name='fetch_near'),
	url(r"^login/$", watch_login(views.login), name="account_login"),	
	url(r'^accounts/', include('allauth.urls')),
	url(r'^ajax/event_create/$', places.views.event_create_modal, name='ajax_event_create'),
]

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)