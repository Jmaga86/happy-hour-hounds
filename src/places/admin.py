from django.contrib import admin

from .models import Place, Day, Event, Neighborhood, Special_type
# Register your models here.

class EventAdmin(admin.ModelAdmin):
    list_display = [
        "__unicode__",
        "name",
        "description",
        "location",
        "time_start",
        "time_end",        
]
    


admin.site.register(Place)
admin.site.register(Day)
admin.site.register(Event, EventAdmin)
admin.site.register(Neighborhood)
admin.site.register(Special_type)