# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-07-28 17:11
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import places.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Day',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('day_of_week', models.CharField(max_length=9)),
                ('day_numbr', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=40)),
                ('slug', models.SlugField(unique=True)),
                ('image', models.ImageField(blank=True, height_field='height_field', null=True, upload_to=places.models.upload_location, width_field='width_field')),
                ('height_field', models.IntegerField(default=0)),
                ('width_field', models.IntegerField(default=0)),
                ('alt_text', models.CharField(blank=True, max_length=255, null=True)),
                ('happy_hour', models.BooleanField()),
                ('event', models.BooleanField()),
                ('special', models.BooleanField()),
                ('featured', models.BooleanField()),
                ('sponsored', models.BooleanField()),
                ('draft', models.BooleanField(default=False)),
                ('time_start', models.TimeField()),
                ('time_end', models.TimeField()),
                ('event_start', models.DateField(blank=True, null=True)),
                ('event_end', models.DateField(blank=True, null=True)),
                ('publish', models.DateField()),
                ('updated', models.DateTimeField(auto_now=True)),
                ('description', models.TextField(blank=True, max_length=255, null=True)),
                ('content', models.TextField(blank=True, max_length=255, null=True)),
                ('day', models.ManyToManyField(to='places.Day')),
            ],
            options={
                'ordering': ['-updated'],
            },
        ),
        migrations.CreateModel(
            name='Event_cat',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('event_cat', models.CharField(max_length=40)),
            ],
        ),
        migrations.CreateModel(
            name='Neighborhood',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=120)),
            ],
        ),
        migrations.CreateModel(
            name='Place',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=120)),
                ('slug', models.SlugField(unique=True)),
                ('image', models.ImageField(blank=True, height_field='height_field', null=True, upload_to=places.models.upload_location, width_field='width_field')),
                ('height_field', models.IntegerField(blank=True, default=0, null=True)),
                ('width_field', models.IntegerField(blank=True, default=0, null=True)),
                ('alt_text', models.CharField(blank=True, max_length=255, null=True)),
                ('address', models.CharField(max_length=120)),
                ('description', models.TextField(blank=True, max_length=255, null=True)),
                ('content', models.TextField(blank=True, null=True)),
                ('neighborhood', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='places.Neighborhood')),
            ],
        ),
        migrations.CreateModel(
            name='Special_type',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('special_type', models.CharField(max_length=40)),
                ('notes', models.TextField(blank=True, max_length=255, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='event',
            name='location',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='places.Place'),
        ),
        migrations.AddField(
            model_name='event',
            name='neighborhood',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='places.Neighborhood'),
        ),
        migrations.AddField(
            model_name='event',
            name='special_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='places.Special_type'),
        ),
    ]
