try:
    from urllib import quote_plus #python 2
except:
    pass

try:
    from urllib.parse import quote_plus #python 3
except: 
    pass
import json
from datetime import date, datetime
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required, permission_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect, Http404, HttpResponseBadRequest
from django.shortcuts import render, render_to_response, get_object_or_404, redirect, RequestContext
from django.utils import timezone
from django.views.generic import View
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from geopy.geocoders import Nominatim
from geopy import geocoders
from geopy.geocoders import Nominatim
from geopy.distance import great_circle

from .functions import find_place, advance_find


from .forms import EventForm, FetchForm, PlaceForm, NeighborhoodForm, Special_typeForm
from blog.models import Post
from .models import Event, Day, Place, Neighborhood, Special_type

def fetch_near(request):
	lat2 = request.POST.get('lat2')
	lng2 = request.POST.get('lng2')
	form = FetchForm(request.GET or None)
	if lat2 != None:
		g = geocoders.GoogleV3()
		find = "%s %s" %(lat2, lng2)
		str_address = g.geocode(find)
		

	if request.method == 'GET':
		g = geocoders.GoogleV3()
		event_list = Place.objects.all()	
		distance = request.GET.get('distance')
		start = request.GET.get("start_time")
		end = request.GET.get("end_time")
		day = request.GET.get("day")
		address = request.GET.get("q")
		geo_loc = g.geocode(address)
		lat2 = (geo_loc.latitude)
		lng2 = (geo_loc.longitude)
		if distance:
			events = event_list.filter(
				Q(event__day__day_numbr=day)&
				Q(event__time_start__lte=start)&
				Q(event__time_end__gte=end)
				).distinct()
			query = (lat2, lng2, distance, start, end, day, events)
			places = advance_find(query)
			
	if request.method == 'POST':

		# lat2 = request.POST.get('lat2')
		# lng2 = request.POST.get('lng2')
		query = (lat2, lng2)
		places = find_place(query)

		
	return render_to_response('hound.html', locals(), context_instance=RequestContext(request)) 


def happyhour_list(request):
	today = timezone.now().date()
	queryset_list = []
	neighborhood_set = Neighborhood.objects.all()
	query = request.GET.get("q")
	if query:
		if not request.user.is_staff or not request.user.is_superuser:
			queryset_list = Place.objects.happy_hour().filter(
					Q(name__icontains=query)|
					Q(description__icontains=query)|
					Q(neighborhood__name__icontains=query)
					).distinct()
		else:
			queryset_list = Place.objects.happy_hour().filter(
					Q(name__icontains=query)|
					Q(description__icontains=query)|
					Q(neighborhood__name__icontains=query)
					).distinct()
	paginator = Paginator(queryset_list, 8) # Show 25 contacts per page
	page_request_var = "page"
	page = request.GET.get(page_request_var)
	try:
		queryset = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		queryset = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		queryset = paginator.page(paginator.num_pages)


	context = {
		"object_list": queryset,
		"title": "Happy Hours",
		"page_request_var": page_request_var,
		"today": today,
		"queryset_list": queryset_list,
		"neighborhood_set": neighborhood_set,
	}
	return render(request, "places/happyhour_list.html", context)

def specials_list(request):
	today = timezone.now().date()
	queryset_list = []
	place_set = Place.objects.all()
	neighborhood_set = Neighborhood.objects.all()
	event_list = Event.objects.special()
	specials_list = Special_type.objects.all()
	query = request.GET.get("q")
	if query:
		if not request.user.is_staff or not request.user.is_superuser:
			queryset_list = Place.objects.special().filter(
					Q(name__icontains=query)|
					Q(description__icontains=query)|
					Q(neighborhood__name__icontains=query)
					).distinct()
		else:
			queryset_list = Place.objects.special().filter(
					Q(name__icontains=query)|
					Q(description__icontains=query)|
					Q(neighborhood__name__icontains=query)
					).distinct()
	paginator = Paginator(queryset_list, 8) # Show 25 contacts per page
	page_request_var = "page"
	page = request.GET.get(page_request_var)
	try:
		queryset = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		queryset = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		queryset = paginator.page(paginator.num_pages)


	context = {
		"object_list": queryset,
		"title": "Specials",
		"page_request_var": page_request_var,
		"today": today,
		"queryset_list": queryset_list,
		"neighborhood_set": neighborhood_set,
		"specials_list": specials_list,
	}

	return render(request, "places/specials_list.html", context)


@login_required	
def events_update(request, slug=None):
	instance = get_object_or_404(Event, slug=slug)
	form = EventForm(request.POST or None, request.FILES or None, instance=instance)
	if form.is_valid():
		instance = form.save()
		instance.save()
		messages.success(request, "<a href='#'>Item</a> Saved", extra_tags='html_safe')
		return HttpResponseRedirect(instance.get_absolute_url())

	context = {
		"title": instance.name,
		"instance": instance,
		"form":form,
	}
	return render(request, "places/events_form.html", context)

class EventsUpdateView(LoginRequiredMixin, UpdateView):
	model = Event
	template_name = "form.html"
	form_class = EventForm

	def get_success_url(self):
		self.place = Place.objects.get(slug=self.kwargs['slug'])
		return reverse("events:place_detail", kwargs={'slug':self.place.slug})

class EventsDeleteView(LoginRequiredMixin, DeleteView):
	model = Event
	template_name = "places/confirm_delete.html"
	
	def get_context_data(self, **kwargs):
		self.place = Place.objects.get(slug=self.kwargs['slug'])
		return super(EventsDeleteView, self).get_context_data(place=self.place, **kwargs)

	def get_success_url(self):
		self.place = Place.objects.get(slug=self.kwargs['slug'])
		return reverse("events:place_detail", kwargs={'slug':self.place.slug})



# Place CRUD

@login_required	
def place_list(request):   
	queryset_list = Place.objects.all()	
	query = request.GET.get("q")
	if query:
		queryset_list = queryset_list.filter(
				Q(name__icontains=query)
				).distinct()
	paginator = Paginator(queryset_list, 8) # Show 25 contacts per page
	page_request_var = "page"
	page = request.GET.get(page_request_var)
	try:
		queryset = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		queryset = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		queryset = paginator.page(paginator.num_pages)


	context = {
		"object_list": queryset,
		"title": "Locations",
		"page_request_var": page_request_var,
		"queryset_list": queryset_list,
	}
	return render(request, "places/place_list.html", context)

def place_detail(request, slug=None):
	instance = get_object_or_404(Place, slug=slug)
	form = EventForm(request.POST or None,
			initial={'location': instance })
	if form.is_valid():
		print form.cleaned_data['time_start']

	context = {
		"form": form,
		"title": instance.name,
		"instance": instance,
	}
	return render(request, "places/place_detail.html", context)

@login_required	
def event_create_modal(request, **kwargs):
	form = EventForm(request.POST or None)
	if request.method == "POST":	
		if form.is_valid():
			instance = form.save()
			instance.save()
			
			return HttpResponse('Success')			
		if form.errors:
			print form.errors
			json_data = json.dumps(form.errors)
			return HttpResponseBadRequest(json_data, content_type='application/json')
		else:
			raise Http404
	context = {
		"form": form,
	}
	return render(request, "places/events_form.html", context)

@login_required
def place_create(request):
	form = PlaceForm(request.POST or None, request.FILES or None)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		messages.success(request, "Successfully Created")
		return HttpResponseRedirect(instance.get_absolute_url())
	context = {
		"form": form,
	}
	return render(request, "places/events_form.html", context)

@login_required
def place_update(request, slug=None):
	instance = get_object_or_404(Place, slug=slug)
	form = PlaceForm(request.POST or None, request.FILES or None, instance=instance)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		messages.success(request, "<a href='#'>Item</a> Saved", extra_tags='html_safe')
		return HttpResponseRedirect(instance.get_absolute_url())

	context = {
		"title": instance.name,
		"instance": instance,
		"form":form,
	}
	return render(request, "places/events_form.html", context)

@login_required
def place_delete(request, slug=None):
	instance = get_object_or_404(Place, slug=slug)
	if request.method =="POST":
		instance.delete()
		messages.success(request, "Successfully deleted")
		return redirect('events:staffevents_list')
	context = {
		"object": instance
	}
	return render(request,"places/confirm_delete.html", context)


# Neighborhood CRUD

def neighborhood_list(request):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404
	queryset_list = Neighborhood.objects.all()	
	query = request.GET.get("q")
	if query:
		queryset_list = queryset_list.filter(
				Q(name__icontains=query)
				).distinct()
	paginator = Paginator(queryset_list, 8) # Show 25 contacts per page
	page_request_var = "page"
	page = request.GET.get(page_request_var)
	try:
		queryset = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		queryset = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		queryset = paginator.page(paginator.num_pages)


	context = {
		"object_list": queryset,
		"title": "Neighborhoods",
		"page_request_var": page_request_var,
		"queryset_list": queryset_list,
	}
	return render(request, "places/neighborhood_list.html", context)

class NeighborhoodCreateView(LoginRequiredMixin, CreateView):
	model = Neighborhood
	login_url = '/login/'
	redirect_field_name = 'redirect_to'	
	template_name = "form.html"
	form_class = NeighborhoodForm
	
	def get_success_url(self):
		return reverse('events:neighborhood_list')

class NeighborhoodUpdateView(LoginRequiredMixin, UpdateView):
	model = Neighborhood
	template_name = "form.html"
	login_url = '/login/'
	redirect_field_name = 'redirect_to'
	form_class = NeighborhoodForm

	def get_success_url(self):
		return reverse("events:neighborhood_list")

class NeighborhoodDeleteView(LoginRequiredMixin, DeleteView):
	model = Neighborhood
	login_url = '/login/'
	template_name = "places/generic_confirm_delete.html"
	redirect_field_name = 'redirect_to'
	def get_success_url(self):
		return reverse("events:neighborhood_list")

class Special_typeCreateView(LoginRequiredMixin, CreateView):
	model = Special_type
	login_url = '/login/'
	redirect_field_name = 'redirect_to'	
	template_name = "form.html"
	form_class = Special_typeForm
	
	def get_success_url(self):
		return reverse('events:special_type_list')

class Special_typeListView(LoginRequiredMixin, ListView):
	model = Special_type
	template_name = "places/special_type_list.html"
	login_url = '/login/'
	redirect_field_name = 'redirect_to'

class Special_typeUpdateView(LoginRequiredMixin, UpdateView):
	model = Special_type
	template_name = "form.html"
	login_url = '/login/'
	redirect_field_name = 'redirect_to'
	form_class = Special_typeForm

	def get_success_url(self):
		return reverse("events:special_type_list")

class SpecialtypeDeleteView(LoginRequiredMixin, DeleteView):
	model = Special_type
	login_url = '/login/'
	redirect_field_name = 'redirect_to'
	template_name = "places/generic_confirm_delete.html"
	def get_success_url(self):
		return reverse("events:special_type_list")

