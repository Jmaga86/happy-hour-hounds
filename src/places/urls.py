from django.conf.urls import url
from django.contrib import admin

from .views import (
	# events_list,
	happyhour_list,
	specials_list,
	EventsUpdateView,
	EventsDeleteView,
	place_list,
	place_create,
	place_detail,
	place_update,
	place_delete,
	neighborhood_list,
	# neighborhood_detail,
	NeighborhoodCreateView,
	NeighborhoodUpdateView,
	NeighborhoodDeleteView,
	Special_typeListView,
	Special_typeCreateView,
	Special_typeUpdateView,
	SpecialtypeDeleteView,
	)

urlpatterns = [
	url(r'^happy_hours$', happyhour_list, name='happyhour_list'),
	url(r'^daily_specials$', specials_list, name='specials_list'),

	url(r'^special_types/$', Special_typeListView.as_view(), name='special_type_list'),
	url(r'^special_types/create/$', Special_typeCreateView.as_view(), name='special_type_create'),
	url(r'^special_types/(?P<pk>\d+)/update/$', Special_typeUpdateView.as_view(), name='special_type_update'),
	url(r'^special_types/(?P<pk>\d+)/delete/$', SpecialtypeDeleteView.as_view(), name='special_type_delete'),	


	url(r'^neighborhood_list/$', neighborhood_list, name='neighborhood_list'),
	url(r'^neighborhood_list/create/$', NeighborhoodCreateView.as_view(), name='neighborhood_create'),
	url(r'^neighborhood_list/(?P<pk>\d+)/edit/$', NeighborhoodUpdateView.as_view(), name='neighborhood_update'),
	url(r'^neighborhood_list/(?P<pk>\d+)/delete/$', NeighborhoodDeleteView.as_view(), name='neighborhood_delete'),

	url(r'^place_list$', place_list, name='place_list'),
	url(r'^place/create$', place_create, name='place_create'),	
	url(r'^(?P<slug>[\w-]+)/$', place_detail, name='place_detail'),
	url(r'^(?P<slug>[\w-]+)/edit/$', place_update, name='place_update'),
	url(r'^(?P<slug>[\w-]+)/delete/$', place_delete, name='place_delete'),	
	
	url(r'^(?P<slug>[\w-]+)/(?P<pk>\d+)/edit/$', EventsUpdateView.as_view(), name='events_update'),
	url(r'^(?P<slug>[\w-]+)/(?P<pk>\d+)/delete/$', EventsDeleteView.as_view(), name='events_delete'),
	

]