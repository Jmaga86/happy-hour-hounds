import urllib2
import json
from datetime import date
from datetime import datetime
from geopy.geocoders import Nominatim
from geopy import geocoders
from geopy.distance import great_circle
from .models import Event, Neighborhood, Place, Day, Special_type



def find_place(query):
	g = geocoders.GoogleV3()
	today = date.today().weekday()
	today_date = date.today()
	locations = Event.objects.filter(day__day_numbr__contains=today).filter(time_start__lte=datetime.now()).filter(time_end__gte=datetime.now()).distinct()
	user = (query)
	places = []
	for location in locations:
			place = g.geocode(location.location.address)
			# event = location.event_set
			pnt = ((place.latitude, place.longitude))
			dist = (great_circle(pnt, user).miles)
			if dist <= 2:
				details = [location.location, place.latitude, place.longitude, location.location.description, location.location.id, location.location.get_absolute_url]
				places.append(details)
	return places



def advance_find(query):
	g = geocoders.GoogleV3()
	today = date.today().weekday()
	locations = (query[6])
	choice = (query[2])
	user = ((query[0]), (query[1]))
	places = []
	for location in locations:
			place = g.geocode(location.address)
			pnt = (place.latitude, place.longitude)
			dist = (great_circle(pnt, user).miles)
			if dist <= int(choice):                
				details = [location, place.latitude, place.longitude, location.description, location.id, location.get_absolute_url]
				places.append(details)               
	return places


