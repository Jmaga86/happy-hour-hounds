from django import forms
from datetime import date, datetime, time
from datetimewidget.widgets import DateTimeWidget, DateWidget, TimeWidget
from .models import Event, Neighborhood, Place, Special_type
from pagedown.widgets import PagedownWidget

def today():
	today = date.today().weekday()
	return today

now = datetime.now().time().isoformat()


DISTANCES = (
	(1, "1"),
	(2, "2"),
	(5, "5"),
	(10, "10"),
	(15, "15"),
	(20, "20"),    
)

DAYS = (
	("", "-----"),
	(0, "Monday"),
	(1, "Tuesday"),
	(2, "Wednesday"),
	(3, "Thursday"),
	(4, "Friday"),
	(5, "Saturday"),
	(6, "Sunday"),
)

TIME = (
	(now, "----"),
	("01:00", "1:00 AM"),
	("02:00", "2:00 AM"),
	("03:00", "3:00 AM"),
	("04:00", "4:00 AM"),
	("05:00", "5:00 AM"),     
	("06:00", "6:00 AM"),
	("07:00", "7:00 AM"),
	("08:00", "8:00 AM"),
	("09:00", "9:00 AM"),
	("10:00", "10:00 AM"),
	("11:00", "11:00 AM"),
	("12:00", "12:00 PM"),     
	("13:00", "1:00 PM"),
	("14:00", "2:00 PM"),
	("15:00", "3:00 PM"),
	("16:00", "4:00 PM"),
	("17:00", "5:00 PM"),
	("18:00", "6:00 PM"),     
	("19:00", "7:00 PM"),
	("20:00", "8:00 PM"),
	("21:00", "9:00 PM"),
	("22:00", "10:00 PM"),
	("23:00", "11:00 PM"),
	("00:00", "12:00 AM"),     
)




class FetchForm(forms.Form):
	search = forms.CharField(
		required=True,
		widget=forms.TextInput(attrs={'required': 'true'}),
	)
	start_time = forms.TimeField(widget=forms.Select(choices=TIME))
	end_time = forms.TimeField(widget=forms.Select(choices=TIME))
	day = forms.IntegerField(widget=forms.Select(choices=DAYS), initial=today,
								  max_value=10)
	distance = forms.IntegerField(widget=forms.Select(choices=DISTANCES), initial=2,
								  max_value=10)
	

class EventForm(forms.ModelForm):
	class Meta:
		model = Event
		fields = [
			"name",
			"description",
			"location",
			"featured",
			"sponsored",
			"special_type",
			"happy_hour",
			"special",
			"day",
			"time_start",
			"time_end",
		]
		widgets = {
			'time_start': TimeWidget(attrs={'class':'datepicker'}, usel10n = True, bootstrap_version=3),
			'time_end': TimeWidget(attrs={'class':'datepicker'}, usel10n = True, bootstrap_version=3),
		}
		
class PlaceForm(forms.ModelForm):
	content = forms.CharField(required=False, widget=PagedownWidget(show_preview=False))
	class Meta:
		model = Place
		fields = [
			"name",
			"content",
			"description",
			"address",
			"neighborhood",
			"image",

		]

class NeighborhoodForm(forms.ModelForm):
	class Meta:
		model = Neighborhood
		fields = [
			"name",

		]
class Special_typeForm(forms.ModelForm):
	class Meta:
		model = Special_type
		fields = [
			"special_type",
			"notes",

		]
