from __future__ import unicode_literals

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save
from django.db import models
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.text import slugify

from markdown_deux import markdown
# Create your models here.

class EventManager(models.Manager):


	def featured(self, *args, **kwargs):
		return super(EventManager, self).filter(featured=True)

	def sponsored(self, *args, **kwargs):
		return super(EventManager, self).filter(sponsored=True)

	def hour(self, *args, **kwargs):
		return super(EventManager, self).filter(happy_hour=True)

	def special(self, *args, **kwargs):
		return super(EventManager, self).filter(special=True)


class PlaceManager(models.Manager):
	def happy_hour(self):
		return super(PlaceManager, self).filter(event__happy_hour=True)

	def special(self):
		return super(PlaceManager, self).filter(event__special=True)	


def upload_location(instance, filename):
	PlaceModel = instance.__class__
	new_id = PlaceModel.objects.order_by("id").last().id + 1
	"""
	instance.__class__ gets the model Post. We must use this method because the model is defined below.
	Then create a queryset ordered by the "id"s of each object, 
	Then we get the last object in the queryset with `.last()`
	Which will give us the most recently created Model instance
	We add 1 to it, so we get what should be the same id as the the post we are creating.
	"""
	return "%s/%s" %(new_id, filename)

class Neighborhood(models.Model):
	name = models.CharField(max_length=120)
	
	class Meta:
		ordering = ['name']
	
	def __unicode__(self):
		return str(self.name)
		
class Place(models.Model):
	name = models.CharField(max_length=120)
	slug = models.SlugField(unique=True)
	image = models.ImageField(upload_to=upload_location, 
		null=True, 
		blank=True, 
		width_field="width_field", 
		height_field="height_field")
	height_field = models.IntegerField(default=0, null=True, blank=True)
	width_field = models.IntegerField(default=0, null=True, blank=True)
	alt_text = models.CharField(max_length=255, null=True, blank=True)	
	address = models.CharField(max_length=120)
	neighborhood = models.ForeignKey(Neighborhood, blank=True, null=True, on_delete=models.CASCADE)	
	description = models.TextField(max_length = 255, blank=True, null=True)
	content = models.TextField(blank=True, null=True)
	
	class Meta:
		ordering = ['name']
	
	objects = PlaceManager()
	
	def __unicode__(self):
		return str(self.name)
	
	def get_absolute_url(self):
		return reverse("events:place_detail", kwargs={"slug": self.slug})
	
	def get_markdown(self):
		content = self.content
		markdown_text = markdown(content)
		return mark_safe(markdown_text)
	
	def get_description(self):
		description = self.description
		markdown_text = markdown(description)
		return mark_safe(markdown_text)

def create_slug(instance, new_slug=None):
	slug = slugify(instance.name)
	if new_slug is not None:
		slug = new_slug
	qs = Place.objects.filter(slug=slug).order_by("-id")
	exists = qs.exists()
	if exists:
		new_slug = "%s-%s" %(slug, qs.first().id)
		return create_slug(instance, new_slug=new_slug)
	return slug


def pre_save_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_slug(instance)

pre_save.connect(pre_save_receiver, sender=Place)
	
class Day(models.Model):
	day_of_week = models.CharField(max_length = 9)
	day_numbr = models.IntegerField()

	def __unicode__(self):
		return (self.day_of_week)

	
class Special_type(models.Model):
	special_type = models.CharField(max_length = 40 )
	notes = models.TextField(max_length = 255, blank=True, null=True)

	class Meta:
		ordering = ['special_type']
	
	def __unicode__(self):
		return (self.special_type)

class Event(models.Model):
	name = models.CharField(max_length = 40)
	location = models.ForeignKey(Place, on_delete=models.CASCADE)
	special_type = models.ForeignKey(Special_type, blank=True, null=True, on_delete=models.CASCADE)
	happy_hour = models.BooleanField(default=False)
	special = models.BooleanField(default=False)
	featured = models.BooleanField(default=False)
	sponsored = models.BooleanField(default=False)   
	day = models.ManyToManyField(Day)
	time_start = models.TimeField(blank=True, null=True)
	time_end = models.TimeField(blank=True, null=True)
	description = models.TextField(max_length = 255, blank=True, null=True)
	
	objects = EventManager()

	def get_markdown(self):
		description = self.description
		markdown_text = markdown(description)
		return mark_safe(markdown_text)

	def __unicode__(self):
		return (self.name)

	def get_absolute_url(self):
		return reverse("events:events_detail", kwargs={"pk": self.pk})
